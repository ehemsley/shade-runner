module.exports = class {
  constructor (gl, errorDisplayer) {
    this.gl = gl
    this.errorDisplayer = errorDisplayer
  }

  _compileShader (source, type) {
    let shader = this.gl.createShader(type)

    this.gl.shaderSource(shader, source)
    this.gl.compileShader(shader)

    if (!this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS)) {
      let error = this.gl.getShaderInfoLog(shader)
      this.errorDisplayer.displayError(error)
      return null
    }

    return shader
  }

  compileVertexShader (source) {
    return this._compileShader(source, this.gl.VERTEX_SHADER)
  }

  compileFragmentShader (source) {
    return this._compileShader(source, this.gl.FRAGMENT_SHADER)
  }

  link (vs, fs) {
    let program = this.gl.createProgram()

    this.gl.attachShader(program, vs)
    this.gl.attachShader(program, fs)

    this.gl.deleteShader(vs)
    this.gl.deleteShader(fs)

    this.gl.linkProgram(program)

    if (!this.gl.getProgramParameter(program, this.gl.LINK_STATUS)) {
      let error = this.gl.getProgramInfoLog(program)
      this.errorDisplayer.displayError(error)
      return null
    }

    return program
  }
}
