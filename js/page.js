const Renderer = require('./renderer.js')
const ErrorDisplayer = require('./error-displayer.js')
const DragAndDropHandler = require('./drag-and-drop-handler.js')
const ShadertoyRetriever = require('./shadertoy-retriever.js')
const ShadertoyGenerator = require('./generators/shadertoy.js')
const FileUtils = require('./file-utils.js')
const TextureUtils = require('./texture-utils.js')
const UI = require('./ui.js')

module.exports = function () {
  let errorContainer = document.getElementById('errorContainer')
  let errorDisplayer = new ErrorDisplayer(errorContainer)
  let rendererContainer = document.getElementById('rendererContainer')
  let shadertoyImportContainer = document.getElementById('shadertoyImportContainer')
  let shadertoyKeyInputContainer = document.getElementById('shadertoyKeyInputContainer')
  let spinnerContainer = document.getElementById('spinnerContainer')
  let shadertoyKeyInput = document.getElementById('shadertoyKey')
  let shadertoyImportErrorContainer = document.getElementById('shadertoyImportErrorContainer')
  let shadertoyImportErrorMessageContainer = document.getElementById('shadertoyImportErrorMessage')

  let renderer = new Renderer(rendererContainer, window.innerWidth, window.innerHeight, errorDisplayer)
  window.requestAnimationFrame(() => { renderer.animationFrameCallback() })

  let dragAndDropHandler = new DragAndDropHandler(renderer, errorDisplayer)

  let ui = new UI({
    shadertoyImportContainer: shadertoyImportContainer,
    shadertoyKeyInputContainer: shadertoyKeyInputContainer,
    shadertoyKeyInput: shadertoyKeyInput,
    spinnerContainer: spinnerContainer,
    shadertoyImportErrorContainer: shadertoyImportErrorContainer,
    shadertoyImportErrorMessageContainer: shadertoyImportErrorMessageContainer
  })

  let commandKeyDown = false

  document.ondragover = document.ondrop = (ev) => {
    ev.preventDefault()
  }

  document.body.ondrop = (ev) => {
    ev.preventDefault()
    dragAndDropHandler.setFileWatch(
      ev.dataTransfer.files[0].path,
      dragAndDropHandler.onReadFragmentCode.bind(dragAndDropHandler),
      dragAndDropHandler.onReadFragmentCode.bind(dragAndDropHandler)
    )
  }

  window.addEventListener('resize', () => {
    renderer.resize(window.innerWidth, window.innerHeight)
  })

  rendererContainer.onclick = rendererContainer.ondrag = (ev) => {
    let x = ev.clientX
    let y = ev.clientY
    renderer.mouseClick(x, y)
  }

  let shadertoyImport = function (key) {
    ui.hideShadertoyKeyInput()
    ui.showShadertoyImportSpinner()

    ShadertoyRetriever.retrieveShader(key, (error, response) => {
      if (error != null) {
        ui.hideShadertoyImportSpinner()
        ui.showShadertoyImportError()
        ui.setShadertoyImportErrorMessage(error)

        setTimeout(() => {
          ui.hideShadertoyImportError()
          ui.showShadertoyKeyInput()
        }, 2000)
      } else {
        // generate shader
        ui.hideShadertoyImport()

        // TODO: stick this callback in the retriever class?
        ui.selectShaderImportDirectory(response.Shader.info.name, (filePaths) => {
          if (filePaths === undefined) return
          let path = filePaths[0]
          console.log(path)
          let shaderFileName = path + '/' + response.Shader.info.name + '.glsl'
          let inputs = response.Shader.renderpass[0].inputs
          for (var input of inputs) {
            if (input.ctype === 'texture') {
              ShadertoyRetriever.handleTextureImport(path, input, (texturePath) => {
                TextureUtils.loadTextureFromURL(renderer.gl, texturePath, (texture, width, height) => {
                  renderer.setChannelTexture(input.channel, texture, width, height)
                })
              })
            }
          }

          let shaderCode = ShadertoyGenerator.fragShader(key, response)
          FileUtils.writeFile(shaderFileName, shaderCode, (err) => {
            if (err) console.log(err)
            FileUtils.setFileWatch(shaderFileName)
            renderer.newShaderCode(shaderCode)
          })
        })
      }
    })
  }

  document.onkeydown = (e) => {
    if (e.keyCode === 91) { // control/command key
      commandKeyDown = true
    }

    if (commandKeyDown) {
      if (e.keyCode === 73) { // i
        ui.toggleShadertoyImportContainer()
      } else if (e.keyCode === 82) { // r
        renderer.resetTime()
      }
    }
  }

  document.onkeyup = (e) => {
    if (e.keyCode === 91) {
      commandKeyDown = false
    }
  }

  shadertoyImportContainer.onkeydown = (e) => {
    if (e.keyCode === 13) { // enter
      let key = shadertoyKeyInput.value

      shadertoyImport(key)
    }
  }
}
