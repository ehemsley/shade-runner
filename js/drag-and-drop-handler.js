module.exports = class {
  constructor (renderer, errorDisplayer) {
    this.renderer = renderer
    this.errorDisplayer = errorDisplayer
  }

  onReadFragmentCode (err, data) {
    if (err) {
      this.errorDisplayer.displayError(err) // send to error handler?
      return null
    }

    let fragmentCode = data
    this.renderer.newShaderCode(fragmentCode) // decouple this?

    return data
  }
}
