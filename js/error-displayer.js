module.exports = class {
  constructor (container) {
    this.container = container
  }

  clearErrors (errorLog) {
    this.container.innerHTML = ''
  }

  _massageErrorLog (errorLog) {
    let massaged = errorLog.replace(/\n/g, '<br>')
    massaged = massaged.replace(/\s0:/g, ' line ')
    return massaged
  }

  displayError (errorLog) {
    this.container.innerHTML = '<p>' + this._massageErrorLog(errorLog) + '</p>'
  }
}
