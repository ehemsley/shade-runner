module.exports = {
  vertShader: `#version 300 es
    precision mediump float;

    attribute vec2 position;
    void main()
    {
      gl_Position = vec4(position, 0.0, 1.0);
    }
  `,

  fragShader: function (key, response) {
    let shaderCode = response.Shader.renderpass[0].code
    shaderCode = shaderCode.replace(/resolution/g, '_myResolution_')
    shaderCode = shaderCode.replace(/time/g, '_myTime_')
    shaderCode = shaderCode.replace(/mouse/g, '_myMouse_')
    shaderCode = shaderCode.replace(/date/g, '_myDate_')
    shaderCode = shaderCode.replace(/sampleRate/g, '_mySampleRate_')
    shaderCode = shaderCode.replace(/channel0/g, '_myChannel0_')
    shaderCode = shaderCode.replace(/channel1/g, '_myChannel1_')
    shaderCode = shaderCode.replace(/channel2/g, '_myChannel2_')
    shaderCode = shaderCode.replace(/channel3/g, '_myChannel3_')
    shaderCode = shaderCode.replace(/channelResolution/g, '_myChannelResolution_')
    shaderCode = shaderCode.replace(/channelTime/g, '_myChannelTime_')
    shaderCode = shaderCode.replace(/offset/g, '_myOffset_')

    shaderCode = shaderCode.replace(/iResolution/g, 'resolution')
    shaderCode = shaderCode.replace(/iTime/g, 'time')
    shaderCode = shaderCode.replace(/iMouse/g, 'mouse')
    shaderCode = shaderCode.replace(/iDate/g, 'date')
    shaderCode = shaderCode.replace(/iSampleRate/g, 'sampleRate')
    shaderCode = shaderCode.replace(/iChannel0/g, 'channel0')
    shaderCode = shaderCode.replace(/iChannel1/g, 'channel1')
    shaderCode = shaderCode.replace(/iChannel2/g, 'channel2')
    shaderCode = shaderCode.replace(/iChannel3/g, 'channel3')
    shaderCode = shaderCode.replace(/iChannelResolution/g, 'channelResolution')
    shaderCode = shaderCode.replace(/iChannelTime/g, 'channelTime')
    shaderCode = shaderCode.replace(/iOffset/g, 'offset')

    return `#version 300 es
// imported from https://www.shadertoy.com/view/${key}

precision mediump float;

uniform vec2      resolution;           // viewport resolution (in pixels)
uniform float     time;           // shader playback time (in seconds)
uniform int       frame;                // frame count
uniform float     frameRate;            // frames per second
uniform vec4      mouse;                // mouse pixel coords
uniform vec4      date;                 // (year, month, day, time in seconds)
uniform float     sampleRate;           // sound sample rate (i.e., 44100)
uniform sampler2D channel0;             // input channel. XX = 2D/Cube
uniform sampler2D channel1;             // input channel. XX = 2D/Cube
uniform sampler2D channel2;             // input channel. XX = 2D/Cube
uniform sampler2D channel3;             // input channel. XX = 2D/Cube
uniform vec3      channelResolution[4]; // channel resolution (in pixels)
uniform float     channelTime[4];       // channel playback time (in sec)
uniform vec2      offset;               // pixel offset for tiled rendering

${shaderCode}

out vec4 fragColor;
void main()
{
  mainImage(fragColor, gl_FragCoord.xy + offset);
}
`
  }
}
