module.exports = {
  vertexShader: `#version 300 es
    precision mediump float;

    in vec2 position;
    void main()
    {
      gl_Position = vec4(position, 0.0, 1.0);
    }
  `,

  surfaceVertexShader: `#version 300 es
    in vec3 position;
    in vec2 surfacePosAttrib;
    out vec2 surfacePosition;
    void main() {
      surfacePosition = surfacePosAttrib;
      gl_Position = vec4( position, 1.0 );
    }
  `,

  fragmentShader: `#version 300 es
    precision mediump float;

    uniform vec2 resolution;
    uniform float time;

    out vec4 fragColor;
    void main()
    {
      vec2 uv = gl_FragCoord.xy / resolution.xy;
      fragColor = vec4(uv, 0.5 + 0.5*sin(time), 1.0);
    }
  `
}
