// TODO: rename to something more generic

const FileUtils = require('./file-utils.js')

function submit (url, loadCallback, errorCallback) {
  let xmlHttp = new XMLHttpRequest()
  xmlHttp.timeout = 2000
  xmlHttp.ontimeout = function () {
    errorCallback('Request timed out.<br>Check your internet connection.')
  }
  xmlHttp.onerror = function () {
    errorCallback(xmlHttp.status)
  }
  xmlHttp.onload = function () {
    loadCallback(JSON.parse(xmlHttp.responseText))
  }
  xmlHttp.open('GET', url, true)
  xmlHttp.send(null)
}

const ShadertoyRetriever = {
  retrieveShader (key, callback) {
    setTimeout(function () {
      let url = `https://www.shadertoy.com/api/v1/shaders/${key}?key=rtrKW1`
      let error = null
      submit(
        url,
        (response) => {
          console.log(url)
          console.log(response)
          if (response.hasOwnProperty('Error')) {
            error = 'Shader not found.<br> Key is incorrect or shader is not available via API.'
            callback(error, response)
          } else {
            callback(null, response)
          }
        },
        (errorCode) => {
          let error = 'Unknown error.'
          if (errorCode === 0) {
            error = 'You are not connected to the internet.'
          }
          callback(error, null)
        }
      )
    }, 400) // minimum response time
  }
}

ShadertoyRetriever.handleTextureImport = function (path, input, callback) {
  FileUtils.makeDirIfNonexistent(path + '/textures')

  let texturePath = path + `/textures/texture${input.channel}.png`
  let imageURL = 'http://shadertoy.com' + input.src
  FileUtils.saveImageFromURL(imageURL, texturePath, (err) => {
    if (err != null) {
      console.log(err)
      return null
    }

    callback(texturePath)
  })
}

module.exports = ShadertoyRetriever
