const FileUtils = require('../js/file-utils.js')
const fs = require('fs-extra')

const chai = require('chai')
const expect = chai.expect

describe('FileUtils', function () {
  describe('image writing', function () {
    let testImageDir = process.cwd() + '/spec/imagetest'
    let testImagePath = testImageDir + '/test.png'

    beforeEach(function () {
      fs.mkdirSync(testImageDir)
    })

    afterEach(function () {
      fs.removeSync(testImagePath)
      fs.rmdirSync(testImageDir)
    })

    describe('writeImageElementToFile', function () {
      it('takes an image element and writes it to file', function (done) {
        let image = new window.Image()
        image.onload = function () {
          FileUtils.writeImageElementToFile(testImagePath, image, function (err) {
            if (err) done(err)
            expect(fs.readdirSync(testImageDir)).to.contain('test.png')
            done()
          })
        }
        image.src = process.cwd() + '/images/blank.png'
      })
    })

    describe('saveImageFromURL', function () {
      let imageURL = process.cwd() + '/images/blank.png'

      it('writes image', function (done) {
        FileUtils.saveImageFromURL(imageURL, testImagePath, function (err) {
          if (err) done(err)
          expect(fs.readdirSync(testImageDir)).to.contain('test.png')
          done()
        })
      })
    })
  })

  describe('setFileWatch', function () {
    // create test file to read
    // TODO: is there a mocha-native way to do this?

    let testDir = process.cwd() + '/spec/testfiles'
    let testFilePath = testDir + '/testfile.txt'

    before(function () {
      fs.mkdirSync(testDir)
      fs.writeFileSync(testFilePath, 'test string')
    })

    after(function () {
      fs.removeSync(testFilePath)
      fs.rmdirSync(testDir)
    })

    it('should read the file', function (done) {
      FileUtils.setFileWatch(testFilePath, function (err, data) {
        if (err) done(err)
        expect(data).to.equal('test string')
        done()
      }, function () {})
    })

    it('should change the title', function () {
      FileUtils.setFileWatch(testFilePath)
      expect(document.title).to.equal('Shade Runner - ' + testFilePath)
    })

    describe('when file changes', function () {
      it('should return data through callback', function (done) {
        FileUtils.setFileWatch(testFilePath, function () {}, function (err, data) {
          if (err) done(err)
          expect(data).to.equal('change test')
          done()
        })

        // TODO: kludge: probably need to use promises
        window.setTimeout(() => {
          fs.writeFileSync(testFilePath, 'change test')
        }, 1000)
      })
    })
  })
})
