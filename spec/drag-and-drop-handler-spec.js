const fs = require('fs-extra')
const DragAndDropHandler = require('../js/drag-and-drop-handler.js')

const Renderer = class {
  constructor () {
    this.fragmentCode = ''
  }

  newShaderCode (code) {
    this.fragmentCode = code
  }
}

const ErrorDisplayer = class {
  constructor () {
    this.error = ''
  }

  displayError (error) {
    this.error = error
  }
}

const chai = require('chai')
const expect = chai.expect

describe('DragAndDropHandler', function () {
  describe('onReadFragmentCode', function () {
    describe('if there was no read error', function () {
      let renderer = new Renderer()
      let errorDisplayer = new ErrorDisplayer()
      let dragAndDropHandler = new DragAndDropHandler(renderer, errorDisplayer)

      it('should send the code to the renderer', function () {
        dragAndDropHandler.onReadFragmentCode(null, 'blah blah blah')
        expect(renderer.fragmentCode).to.equal('blah blah blah')
      })
    })

    describe('if there was a read error', function () {
      let renderer = new Renderer()
      let errorDisplayer = new ErrorDisplayer()
      let dragAndDropHandler = new DragAndDropHandler(renderer, errorDisplayer)

      it('should log the error', function () {
        dragAndDropHandler.onReadFragmentCode('ERROR: your butt smells', 'blah blah blah')
        expect(errorDisplayer.error).to.equal('ERROR: your butt smells')
      })

      it('should return null', function () {
        expect(dragAndDropHandler.onReadFragmentCode('ERROR: your butt smells', 'blah blah blah')).to.be.null
      })
    })
  })
})
