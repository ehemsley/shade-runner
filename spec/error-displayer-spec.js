const ErrorDisplayer = require('../js/error-displayer.js')

const chai = require('chai')
let expect = chai.expect

describe('ErrorDisplayer', function () {
  let errorDisplayContainer = document.createElement('div')
  let errorDisplayer = new ErrorDisplayer(errorDisplayContainer)

  describe('clearErrors', function () {
    errorDisplayer.clearErrors()

    it('should clear the display', function () {
      expect(errorDisplayContainer.innerHTML).to.equal('')
    })
  })

  describe('_massageErrorLog', function () {
    it('should replace newlines with br', function () {
      let errorLog = 'hey\ndude'
      expect(errorDisplayer._massageErrorLog(errorLog)).to.equal('hey<br>dude')
    })

    it('should replace \'\\s0:\' with \'line \'', function () {
      let errorLog = 'ERROR: 0:12: your butt is smelly'
      expect(errorDisplayer._massageErrorLog(errorLog)).to.equal('ERROR: line 12: your butt is smelly')
    })
  })

  describe('displayError', function () {
    it('should display error in container', function () {
      let errorLog = 'ERROR: 0:12: your butt is smelly\nERROR: 0:14: your feet are also smelly'
      errorDisplayer.displayError(errorLog)
      expect(errorDisplayContainer.innerHTML).to.equal('<p>ERROR: line 12: your butt is smelly<br>ERROR: line 14: your feet are also smelly</p>')
    })
  })
})
