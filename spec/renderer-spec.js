const Renderer = require('../js/renderer.js')

const chai = require('chai')
let expect = chai.expect

describe('Renderer', function () {
  let errorHandler = { clearErrors: function () {} }
  let renderer = new Renderer(document.body, 800, 600, errorHandler)

  describe('resize', function () {
    renderer.resize(300, 200)

    it('should resize elements properly', function () {
      expect(renderer.width).to.equal(300)
      expect(renderer.height).to.equal(200)
      expect(renderer.container.width).to.equal(300)
      expect(renderer.container.height).to.equal(200)
      expect(renderer.canvas.width).to.equal(300)
      expect(renderer.canvas.height).to.equal(200)
    })
  })

  describe('mouseClick', function () {
    renderer.mouseClick(200, 100)

    it('records mouse position', function () {
      expect(renderer.mouseClickPosition.x).to.equal(200)
      expect(renderer.mouseClickPosition.y).to.equal(100)
    })
  })

  describe('createTarget', function () {
    let target = renderer._createTarget(300, 200)

    it('should return a proper render target', function () {
      expect(target.framebuffer).to.be.a('webglframebuffer')
      expect(target.renderbuffer).to.be.a('webglrenderbuffer')
      expect(target.texture).to.be.a('webgltexture')
    })
  })
})
